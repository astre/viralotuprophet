<center> <h1>ViralOtuProphet</h1> </center>

This script is part of a pipeline for **analyzing virus taxonomic data derived from BLAST results** and count tables. 
It performs a series of filtering, clustering, and merging operations to process the data and prepare it for downstream analysis. 
The clustering is based on **label propagation algorithm**, which assigns each contig to a cluster based on the labels of its neighboring contigs. First we constructs an undirected graph where each node represents a contig and edges are weighted based on the similarity of the contigs homologies.The resulting clusters represent groups of contigs with similar count profiles across samples.

## Requirements

To run this script, you need to have the following R packages installed:

- dplyr
- purrr
- widyr
- stringr
- tidyr
- ggplot2
- igraph

You can install these packages using the install.packages() function in R.

## Input Data

The script expects the following input data files to be present in the working directory:

- BLAST table (tab-separated values file)
- Virus taxonomy table (CSV file)
- Hosts taxonomy table (CSV file)
- Count table (CSV file)
- Coverage range table (CSV file)
- Coverage stats table (CSV file)

The script assumes that the BLAST table contains columns  named :

'qseqid', 'sseqid', 'pident', 'length', 'mismatch', 'gapopen', 'qstart', 'qend', 'sstart', 'send', 'evalue', and 'bitscore'. 

The count table should have row names representing contig IDs and column names representing sample IDs.

## Functions

The script includes several functions to process the input data:

- fblastscore: Filters BLAST results based on percentage identity and bitscore.
- fcover: Filters BLAST results based on coverage and whether the alignment is in a low-coverage region.
- clustcount: Performs clustering based on count data.
- clustma: Performs clustering based on a graph derived from the count data.
- mergetaxo: Merges taxonomic information based on the clustering results.
- cleaned_merge: Cleans and merges the BLAST results and count data.
- dedupl: Removes duplicated species with multiple tax_id.
- fctrl: Filters control data from the count table.

## Usage

Set the working directory to the location of the input data files using the setwd() function.
Load the input data files using the appropriate R functions (e.g., read.csv(), read.table()).
Run the functions in the order specified in the script to process the data.
Save the output data as CSV files using the write.csv() function.
Output Data
The script generates the following output data files:

Filtered BLAST table (CSV file)
Filtered taxonomy table (CSV file)
Filtered count table (CSV file)
Clustering results (CSV file)
Merged taxonomic information (CSV file)
The output data can be used for downstream analysis, such as visualization, statistical testing, and interpretation of the results.

## Note

This script is intended for use with virus taxonomic data, but it may be adapted for other types of data with appropriate modifications. The script assumes that the input data is properly formatted and preprocessed. It is the user's responsibility to ensure the quality and integrity of the input data.